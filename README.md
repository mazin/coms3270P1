# coms3270P1

# features:

1. calculate minimum, maximum and average height then proceed to print it.
2. Take the height of a given file

# known errors and issues:

1. if the file does not have 3 numbers per row then it might not execute properly
2. if the file is empty

#To execute:

# 1) make

a) will create an object pointcloud.o

# 2) ./state < test.xyz

a) change test.xyz to test file when needed

# 3) see the output
